# Install Arch

### Create bootable USB

- Follow instructions on https://scoop.sh/ to install scoop

- Install required software:

```bash
scoop install git
scoop bucket add extras
scoop install extras/qbittorrent extras/usbimager

```

- Download [Arch](https://archlinux.org/download/) using the magnet link
- Burn the Arch ISO image using usbimager

### Boot from USB

- Select first option
- Use install wizard:

```bash
archinstall
```

- Configure following:

  - Disk configuration
    - Use a best-effort default partition layout
    - Select target drive
    - Select ext4
    - Do not create a separate partition for /home
  - Hostname
    - Change to a desired value, such as 'homeserver'
  - Root password
    - Select root password you will remember
  - User account
    - Create an admin account
  - Profile - select 'Minimal'
  - Network configuration - select 'Copy ISO network configuration to installation'

- Select 'Install'
- Reboot

### Enable SSH

```bash
sudo pacman -S openssh
sudo systemctl enable --now sshd
```

### Install git and clone the repo

```bash
sudo pacman -S --noconfim git
mkdir ~/code
cd ~/code
git clone https://gitlab.com/kksiuda/homeserver
cd homeserver
```

### Install using individual scripts

```bash

sudo sh install_[app].sh

```
