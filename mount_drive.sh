#!/bin/bash

source ./lib.sh

function mount_drive {
    local uuid=$(sudo blkid -o value -s UUID /dev/sdb1)
    if ! cat /etc/fstab | grep -q $uuid
    then
        install_package ntfs-3g
        echo -e "UUID=$uuid/mnt/hdd\tntfs-3g\tnoauto,x-systend.automount\t0 0" > /etc/fstab
        systemctl daemon-reload
        mount -a
    fi
}

