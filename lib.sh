#!/bin/bash

#################################
#           General             #
#################################


function get_random_password {
    head -c 32 /dev/urandom | sha1sum | cut -d' ' -f1
}

function copy_template_as {
    sudo -u $1 cat $2 | envsubst > $3
}

#################################
#           Packages            #
#################################

function is_command_available {
    command -v $1
}

function is_package_installed {
    pacman -Q $1 > /dev/null 2>&1
}

function install_package {
    if ! is_package_installed
    then
        pacman -S --noconfirm $1
    fi
}

function init_yay {
    if ! is_command_available yay
    then
        if ! cat /etc/passwd | grep -q aur
        then
            useradd aur -m
        fi    
        install_package git 
        install_package base-devel 
        install_package go  
        cd /home/aur
        sudo -u aur git clone https://aur.archlinux.org/yay.git
        cd yay
        sudo -u aur makepkg -s
        pacman -U $(ls *.zst) --noconfirm
        yay -Y --gendb
        yay -Syu --devel
        yay -Y --devel --save
    fi

}

function install_aur_package {
    init_yay
    yay -a -S --noconfirm $1 
}

#################################
#           Services            #
#################################


function service_is_active {
    systemctl status $1 | grep -q "Active: active" > /dev/null 2>&1
}

function enable_service {
    systemctl enable --now $1
}

function restart_service {
    systemctl restart $1
}

function check_service {
    if service_is_active $1
    then
        echo "[SKIP] $1 service already running"
        exit 1
    fi
}

function verify_service {
    if service_is_active $1
    then
        echo "[SUCCESS] $1 service installed"
        exit 0
    else
        echo "[ERROR] $1 service not installed"
        exit 1
    fi
}

#################################
#           Postgres            #
#################################


function postgres_initiate {
    if ! service_is_active postgresql
    then
        install_package postgresql
        sudo -u postgres initdb --locale=C.UTF-8 --encoding=UTF8 -D /var/lib/postgres/data --data-checksums
        enable_service postgresql
    fi
}

function postgres_create {
    postgres_initiate
    sudo -u postgres createuser $1
    sudo -u postgres createdb -O $1 $2
}


#################################
#            Input              #
#################################

function read_password {
    systemd-ask-password -n --emoji=no "$1"
}

function read_input {
    read -p "$1" __TMPINPUT
    echo $__TMPINPUT
    unset $__TMPINPUT
}