#!/bin/bash

source ./lib.sh
source ./ports.sh

check_service mumble-server
install_package mumble-server
export MUMBLE_PASSWORD=$(read_password "Server password: ")
copy_template_as _mumble-server ./templates/mumble/mumble-server.ini /etc/mumble/mumble-server.ini
mkdir -p /var/run/mumble-server
sudo chown -R _mumble-server /var/run/mumble-server
enable_service mumble-server
verify_service mumble-server