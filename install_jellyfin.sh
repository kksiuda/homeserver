#!/bin/bash

source ./lib.sh
source ./mount_drive.sh

check_service jellyfin
mount_drive
install_package jellyfin-server
install_package jellyfin-web
enable_service jellyfin
verify_service jellyfin