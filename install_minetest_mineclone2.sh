#!/bin/bash

source ./lib.sh
source ./ports.sh

export MINETEST_GAME="mineclone2"
export MINETEST_GAME_LINK="https://content.minetest.net/packages/Wuzzy/mineclone2/releases/22662/download/"
export MINETEST_GAME_VERSION="0.86.2"
export MINETEST_GAME_PORT=30000
export MINETEST_ADMIN=$(read_input "Admin account: ")

check_service minetest@$MINETEST_GAME
install_package minetest-server
chown -R minetest /usr/share/minetest /etc/minetest
sudo -u minetest curl -L $MINETEST_GAME_LINK -o /tmp/game.zip
sudo -u minetest mkdir -p /usr/share/minetest/games/$MINETEST_GAME
sudo -u minetest mkdir -p /usr/share/minetest/worlds/$MINETEST_GAME
sudo -u minetest bsdtar -xf /tmp/game.zip -C "/usr/share/minetest/games/"
sudo -u minetest rm /tmp/game.zip
copy_template_as minetest ./templates/minetest/$MINETEST_GAME/server.conf /etc/minetest/$MINETEST_GAME.conf
copy_template_as minetest ./templates/minetest/$MINETEST_GAME/world.mt usr/share/minetest/worlds/$MINETEST_GAME/world.mt
enable_service minetest@$MINETEST_GAME
verify_service minetest@$MINETEST_GAME