#!/bin/bash

source ./lib.sh

check_service pihole-FTL
install_aur_package pi-hole-server
mkdir -p /etc/systemd/resolved.conf.d
cp ./templates/pihole/resolved-override.conf /etc/systemd/resolved.conf.d/pi-hole.conf
restart_service systemd-resolved
enable_service pihole-FTL
verify_service pihole-FTL