#!/bin/bash

source ./lib.sh
source ./ports.sh

export NZBGET_PASSWORD=$(get_random_password)

#################################
#            NZBGet             #
#################################

if ! service_is_active nzbget
    install_aur_package nzbget-git
    chown -R nzbget:nzbget /var/lib/nzbget
    chmod -R 750 /var/lib/nzbget
    copy_template_as nzbget ./templates/nzbget/nzbget.conf /var/lib/nzbget/.nzbget
    enable_service nzbget
else
    copy_template_as nzbget ./templates/nzbget/nzbget.conf /var/lib/nzbget/.nzbget
    restart_service nzbget
fi

if ! service_is_active nzbget
    echo "Failed to install NZBGet service"
    exit 1
fi

