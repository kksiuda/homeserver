#!/bin/bash

source ./lib.sh
source ./ports.sh

export GITEA_DB_USERNAME=gitea
export GITEA_DB_DATABASE=gitea
export GITEA_SECRET=$(get_random_password)
export GITEA_TOKEN=$(get_random_password)

check_service gitea
install_package gitea
postgres_create $GITEA_DB_USERNAME $GITEA_DB_DATABASE
copy_template_as gitea ./templates/gitea/app.ini /etc/gitea/app.ini
enable_service gitea
verify_service gitea
